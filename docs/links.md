🌍 Links

## To read

- [WasmEdge Hyper Demo (embed an http server + client)](https://github.com/WasmEdge/wasmedge_hyper_demo)
- [Getting started with docker and wasm](https://nigelpoulton.com/getting-started-with-docker-and-wasm/)
- [Standardizing WASI: A system interface to run WebAssembly outside the web](https://hacks.mozilla.org/2019/03/standardizing-wasi-a-webassembly-system-interface/)
- [WebAssembly and Docker - Better Together](https://github.com/collabnix/wasm-docker-better-together)
- [Run your workers anywhere with WebAssembly](https://wasmlabs.dev/articles/run-workers-anywhere/)

## Runtimes

- [WasmEdge](https://wasmedge.org/)
- [WasmTime](https://wasmtime.dev/)
- [Wazero](https://wazero.io/)
- [Wasmer](https://wasmer.io/)

## Frameworks

- [Extism](https://extism.org/)
- [Spin](https://developer.fermyon.com/spin/index)
- [E2Core](https://github.com/suborbital/e2core)
- [Capsule](https://bots-garden.github.io/capsule/)
- [Wasm Workers Server](https://workers.wasmlabs.dev/)
