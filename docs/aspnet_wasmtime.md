# Run ASP.Net inside WASM with WASI and WasmTime

!!! info "About this post"
    - Created: 2022 November 9th

.NET 7 is [Available](https://devblogs.microsoft.com/dotnet/announcing-dotnet-7/) and there is a preview WASI support for dotNet Core and ASP.Net. It's very easy to getting started.

## Setup

### Install .NET 7

- Go on https://dotnet.microsoft.com/en-us/download/dotnet/7.0 and download the appropriate SDK for your OS
- Install it

### Install WasmTime

```bash
curl https://wasmtime.dev/install.sh -sSf | bash
```

## Generate a new ASP.Net project

```bash
dotnet new web -o hello
```

```bash title="output"
hello
├── appsettings.Development.json
├── appsettings.json
├── hello.csproj
├── obj
│  ├── hello.csproj.nuget.dgspec.json
│  ├── hello.csproj.nuget.g.props
│  ├── hello.csproj.nuget.g.targets
│  ├── project.assets.json
│  └── project.nuget.cache
├── Program.cs
└── Properties
   └── launchSettings.json
```


### Install the dotNet WASI support for the project

```bash
cd hello
dotnet add package Wasi.Sdk --prerelease
dotnet add package Wasi.AspNetCore.Server.Native --prerelease
```
!!! warning
    The released packages exist but the build will fail if you use it

### Change the source code of `Program.cs`


```csharp title="Update `Program.cs`"
using System.Runtime.InteropServices;

var builder = WebApplication.CreateBuilder(args).UseWasiConnectionListener();

var app = builder.Build();

app.MapGet("/", () => {
  return $"👋 Hello, World! 🌍 🖥️: {RuntimeInformation.OSArchitecture} ⏳: {DateTime.UtcNow.ToLongTimeString()} (UTC)";
});

app.Run();
```

## Build and run

```bash title="build"
cd hello
dotnet build
ls -lh bin/Debug/net7.0/*.wasm
```

```bash title="run"
cd hello
wasmtime bin/Debug/net7.0/hello.wasm --tcplisten localhost:8080
```

![aspnet_wasmtime_00](imgs/aspnet_wasmtime_00.png)


!!! note "Some readings on the same topic"
    - [Embed HTTP servers in WASM with Rust and CSharp](https://blog.kubesimplify.com/embed-http-servers-in-wasm-with-rust-and-csharp) by [@k33g_org](https://twitter.com/k33g_org)
    - [Using WASM and WASI to run .NET 7 on a Raspberry PI Zero 2 W](https://laurentkempe.com/2022/10/29/using-wasm-and-wasi-to-run-dotnet-7-on-a-raspberry-pi-zero-2-w/) by [@laurentkempe](https://twitter.com/laurentkempe)
    - [Experimenting with .NET 7, WASM, and WASI on Docker](https://laurentkempe.com/2022/10/31/experimenting-with-dotnet-7-wasm-and-wasi-on-docker/) by [@laurentkempe](https://twitter.com/laurentkempe)








