# 🧾 Issues to follow

- [HTTP server embedded mode works with WasmEdge and not with WasmTime (and vice et versa)](https://github.com/WasmEdge/WasmEdge/issues/2056)
