# 📝 External Blog posts

- [Embed HTTP servers in WASM with Rust and CSharp](https://blog.kubesimplify.com/embed-http-servers-in-wasm-with-rust-and-csharp)
- [Extend Wasm with host functions thanks to Wazero](https://www.wasm.builders/k33g_org/extend-wasm-with-host-functions-thanks-to-wazero-3n0n)
- [An essay on the bi-directional exchange of strings between the Wasm module (with TinyGo) and Node.js (with WASI support)](https://www.wasm.builders/k33g_org/an-essay-on-the-bi-directional-exchange-of-strings-between-the-wasm-module-with-tinygo-and-nodejs-with-wasi-support-3i9h)

